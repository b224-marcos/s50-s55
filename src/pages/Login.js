import { useState, useEffect, useContext } from 'react'
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'


export default function Login() {

	// Allows us to consume the User context object and its properties to be used for validation
	const { user, setUser } = useContext(UserContext)

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	
	const [isActive, setIsActive] = useState(false);

	// console.log(email);
	console.log(password);

	function logInUser(e) {
		
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})

		.then(res => res.json())
		.then(data => {
			// We will receive either a token or a false response
			console.log(data)

			if (typeof data.access !== "undefined") {
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Zuitt"
				})

			} else {
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Please check your details and try again"
				})
			}
			
		})



		// Set email of the authenticated user in the local storage.

		/*
			Syntax:
			 localStorage.setItem("propertyName", value)
		*/

		// localStorage.setItem("email", email)

		// Sets the global user state to have properties obtained from local storage
		// setUser({email: localStorage.getItem('email')})

		// Clearing the input fields and states
		setEmail("");
		setPassword("");

		//alert("You are now logged in!")
	}

	
	const retrieveUserDetails = (token) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}

		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// Global user state for validation across the whole app
			setUser ({
				id: data._id,
				isAdmin: data.isAdmin
			})

		})
	}

	

	useEffect(() => {
		if (email !== "" && password !== "") {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password]);

	return (
		
		(user.id !== null) ? 
		<Navigate to="/courses" />
		:
		<Form onSubmit={(e) => logInUser(e)}>
			<Form.Group className="mb-3" controlId="userEmail">
        		<Form.Label>Email</Form.Label>
        		<Form.Control 
        			type="email" 
        			value={email}
        			onChange={(e) => {setEmail(e.target.value)}}
        			placeholder="Enter Email" />
      		</Form.Group>

      		<Form.Group className="mb-3" controlId="password">
        		<Form.Label>Password</Form.Label>
        		<Form.Control 
        			type="password"
        			value={password}
        			onChange={(e) => {setPassword(e.target.value)}} 
        			placeholder="Enter Password" />
      		</Form.Group>
      		{ isActive ?
      			<Button variant="success" type="submit" id="submitBtn">
        		Login
      			</Button>
      			:
      			<Button variant="success" type="submit" id="submitBtn" disabled>
        		Login
      			</Button>
      		}
      	</Form>
	)
};
