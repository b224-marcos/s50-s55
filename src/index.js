import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';



// Import the Bootstrap CSS 
import 'bootstrap/dist/css/bootstrap.min.css';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);



/*const name = "Ronie John Timbal"

const student = {
  firstName : "Rico",
  lastName: "Tasong"
}

function userName(user) {
  return user.firstName + '' + user.lastName
}

let element = <h1>Hello, {userName(student)} </h1>
element = <h1>Hello, {name} </h1>

root.render(element);*/


